var Q = require('q');
var plaid = require('plaid');

var STRIPE_API_SECRET_KEY = process.env.STRIPE_API_SECRET_KEY;
var stripe = require("stripe")(STRIPE_API_SECRET_KEY);

var FUNDING_TYPE = {
  CARD: 1,
  BANK: 2
};

module.exports = function(Deposit) {

  Deposit.beforeRemote('chargeBank', function (context, unused, next) {
    var req = context.req;
    if(req.accessToken) {
      req.remotingContext.args.accessToken = req.accessToken;
      next();
    }
    else {
      var authError = new Error('Unauthorized');
      authError.statusCode = 401;
      authError.code = 'Unauthorized';
      next(authError);
    }
  });


  Deposit.chargeBank = function chargeBank(accessToken, amount, cb) {
    console.log("phone: " + accessToken.userId);
      console.log("amount: " + JSON.stringify(amount));
      
      var teller =  {
        phone: accessToken.userId
      };
      var sourceId = amount.sourceId;
      var charge;
      var deposit;

      getStripeCustId(teller)
      .then(function(profile) {
        teller.stripeCustomerId = profile.stripeCustomerId;
        return stripeCharge(teller, amount);
      })
      .then(function(stripeCharge) {
        console.log("charge: " + JSON.stringify(stripeCharge));
        charge = stripeCharge;
        var bank = stripeCharge.source;
        return getStripeBalance(charge);
      })
      .then(function(balance) {
        // Uses provided amounts from app which will be in foreign currency
        return logDeposit(teller, amount, sourceId);
        // use this for USD deposits to get balance directly from stripe API
        //return log(teller, balance, sourceId); 
      })
      .then(function(loggedDeposit) {
        deposit = loggedDeposit;
        console.log("deposit: " + JSON.stringify(deposit));
        return updateBalance(teller, deposit);
      })
      .then(function(updatedTeller) {
        // make sure this is returning right phone
        teller = updatedTeller;
      })
      .catch(function(error) {
        console.log("Error: " + error);
        cb(error);
      })
      .done(function() {
        cb(null, {
          teller: teller,
          deposit: deposit
        });
      });
  };

  // expose the above method through the REST
    Deposit.remoteMethod('chargeBank', {
      description: 'Stripe charge',
      accepts: [
            {arg: 'accessToken', type: 'object'},
            {arg: 'amount', type: 'object', required: true, http: {source: 'body'}}
        ],
      returns: {
          arg: 'stripe',
          type: 'object',
          root: true
      },
      http: {
          path: '/charge-bank',
          verb: 'post'
      }
  });

  function getFundingSource(teller, stripeId) {
    var deferred = Q.defer();

    Deposit.app.models.Source.findOne({
      where: {
        phone: teller.phone,
        stripeId: stripeId
      }
    },
    function(err, source) {
      if(err) {
        console.log("getFundingSource error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("getFundingSource:", source);
        deferred.resolve(source);
      }
    });

    return deferred.promise;
  }

  function getBankDetails(stripeCustId, stripeBankId) {
    var deferred = Q.defer();

    stripe.customers.retrieveSource(stripeCustId, stripeBankId,
      function(err, bank) {
        if (err) {
          console.log("getBankDetails err: " + JSON.stringify(err));
          deferred.reject(new Error("Error Retrieving Bank Details"));
        } 
        else {
          console.log("getBankDetails: " + JSON.stringify(bank));
          deferred.resolve(bank);
        } 
      }
    )

    return deferred.promise;
  }

  Deposit.beforeRemote('chargeCard', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Deposit.chargeCard = function chargeCard(accessToken, amount, cb) {

      console.log("phone: " + accessToken.userId);
      console.log("amount: " + JSON.stringify(amount));
      
      var teller =  {
        phone: accessToken.userId
      };
      var sourceId = amount.sourceId;
      var charge;
      var card;
      var deposit;

      getStripeCustId(teller)
      .then(function(profile) {
        teller.stripeCustomerId = profile.stripeCustomerId;
        return stripeCharge(teller, amount);
      })
      .then(function(stripeCharge) {
        console.log("charge: " + JSON.stringify(stripeCharge));
        charge = stripeCharge;
        card = stripeCharge.source;
        return getStripeBalance(charge);
      })
      .then(function(balance) {
        // Uses provided amounts from app which will be in foreign currency
        return logDeposit(teller, amount, sourceId);
        // use this for USD deposits to get balance directly from stripe API
        //return log(teller, balance, sourceId); 
      })
      .then(function(loggedDeposit) {
        deposit = loggedDeposit;
        console.log("deposit: " + JSON.stringify(deposit));
        return updateBalance(teller, deposit);
      })
      .then(function(updatedTeller) {
        // make sure this is returning right phone
        teller = updatedTeller;
        Q.when({});
      })
      .catch(function(error) {
        console.log("Error: " + error);
        cb(error);
      })
      .done(function() {
        cb(null, {
          teller: teller,
          deposit: deposit
        });
      });
    };

    function getStripeCustId(teller) {
      var deferred = Q.defer();

      Deposit.app.models.Profile.findOne({phone: teller.phone} , function(err, profile) {
        if(err) {
          console.log("get profile callback: error:", err)
          deferred.reject(new Error(err));
        } else {
          console.log("get profile callback:", profile);
          if (profile) deferred.resolve(profile);
          deferred.resolve(null);
        }
      });

      return deferred.promise;
    }

    function stripeCharge(teller, amount) {
      var deferred = Q.defer();

      var options = {
        amount:           amount.stripeAmount,
        currency:         amount.stripeCurrency,
        description:      amount.stripeDescription,
        customer:         teller.stripeCustomerId
      };

      if (amount.stripeCardId) options.source = amount.stripeCardId;
      if (amount.stripeBankId) options.source = amount.stripeBankId;

      stripe.charges.create(options, function(err, charge) {

        if(err) {
          console.log("stripe callback: error:", err)
          deferred.reject(new Error(err));
        } else {
          console.log("stripe callback: charge:", charge)
          deferred.resolve(charge);
        }
          
      });

      return deferred.promise;
    }

    function getStripeBalance(charge) {
      var deferred = Q.defer();

      stripe.balance.retrieveTransaction(
        charge.balance_transaction,
        function(err, balance) {
          if (err) deferred.reject(new Error(err));
          else {
            console.log("stripe callback: balance:", balance)
            deferred.resolve(balance);
          }
        }
      );

      return deferred.promise;
    }

    function logDeposit(teller, amount, sourceId) {
      var net = amount.stripeAmount - amount.stripeFee;

      var balance = {
        amount: amount.stripeAmount,
        net: net,
        fee: amount.stripeFee,
        currency: amount.stripeCurrency
      };

      return log(teller, balance, sourceId);
    }

    function log(teller, balance, sourceId) {
      var deferred = Q.defer();

      var amount = balance.amount;
      var net = balance.net;
      var fee = balance.fee;

      Deposit.create({ phone: teller.phone, amount: amount, net: net, fee: fee, currency: balance.currency, sourceId: sourceId }, function(err, deposit) {
        console.log("deposit: " + JSON.stringify(deposit));
        if (err) deferred.reject(err);
        else deferred.resolve(deposit);
      });

      return deferred.promise;
    }

    function updateBalance(teller, deposit) {
      var deferred = Q.defer();

      Deposit.app.models.Teller.findOne({ where: { phone: teller.phone }}, function(err, t) {
      console.log("t: " + JSON.stringify(t));
      if (err) deferred.reject(err);
      else {
        console.log("balance = " + t.balance + " + " + deposit.net);
        balance = t.balance + deposit.net;
        //balance = ""+balance;

        Deposit.app.models.Teller.upsert({ phone: teller.phone, cc: t.cc, balance: balance }, function(err, updatedTeller) {
          console.log("updatedTeller: " + JSON.stringify(updatedTeller));
          if (err) deferred.reject(err);
          else  {
            deferred.resolve(updatedTeller);
          }
        });
      }
    });

      return deferred.promise;
    }

    // expose the above method through the REST
    Deposit.remoteMethod('chargeCard', {
        description: 'Stripe charge',
        accepts: [
              {arg: 'accessToken', type: 'object'},
              {arg: 'amount', type: 'object', required: true, http: {source: 'body'}}
          ],
        returns: {
            arg: 'stripe',
            type: 'object',
            root: true
        },
        http: {
            path: '/charge-card',
            verb: 'post'
        }
    });

};
