var speakeasy = require('speakeasy');

var loopback = require('loopback');
var app = require('../../server/server');
 
var click = require('../utils/clickatell');

module.exports = function(Teller) {
  delete Teller.validations.password;
  delete Teller.validations.email;
  // Teller.validations.email = Teller.validations.email.filter(function(rule) {
  //   return (rule.validation !== 'presence'); // && (rule.validation !== 'format');
  // });

  Teller.definition.properties.password.default = 
  Teller.definition.properties.email.default =function() {
    return "";
  };

  Teller.definition.rawProperties.created.default =
  Teller.definition.properties.created.default = function() {
    return new Date();
  };

  Teller.validatePassword = function(plain) {
    return true;
  }


  var APP_SECRET = process.env.APP_SECRET; // "asdf9819238498213984asdfsjdfjkljlzio!as";

  Teller.prototype.createAccessToken = function(ttl, options, cb) {
    if (cb === undefined && typeof options === 'function') {
      // createAccessToken(ttl, cb)
      cb = options;
      options = undefined;
    }

    cb = cb || utils.createPromiseCallback();

    if (typeof ttl === 'object' && !options) {
      // createAccessToken(options, cb)
      options = ttl;
      ttl = options.ttl;
    }
    options = options || {};
    var userModel = this.constructor;
    ttl = Math.min(ttl || userModel.settings.ttl, userModel.settings.maxTTL);
    this.accessTokens.create({
      userId: options.tokenParams.phone,
      phone: options.tokenParams.phone,
      cc: options.tokenParams.cc,
      ttl: ttl
    }, cb);
    return cb.promise;
  };

  // By overriding the default `login()` method here we can 
  // restrict the UI from "accidentally" trying to hit the /api/Tellers/login route
  Teller.login = function(credentials, include, fn) {
      fn(new Error('This application requires phone number authentication.'));
  };

  /**
   * Request a two-factor authentication code for use during the login process
   * NOTE: this does NOT send the code anywhere, that is left as an experiment 
   *       for the reader of this example code. :)
   * 
   * @param  {object}   credentials A JSON object containing "phone" and "cc" fields
   * @param  {Function} fn          The function to call in the Loopback for sending back data
   * @return {void}
   */
  Teller.requestCode = function(credentials, fn) {
      var self = this,
          now = (new Date()).getTime(),
          defaultError = new Error('Login Failed');
      
      defaultError.statusCode = 401;
      defaultError.code = 'LOGIN_FAILED';
      
      // if (!credentials.phone || !credentials.cc || !credentials.uuid) {
      if (!credentials.phone || !credentials.cc) {
        return fn(new Error('Login Failed: Missing Param'));
      }

      console.log("credentials:" + JSON.stringify(credentials));

      self.findOne({where: { phone: credentials.phone }}, function(err, teller) {
        console.log("teller: " + JSON.stringify(teller));
        if (err) return fn(err);
        // if (teller && teller.uuid === credentials.uuid) {
        if (teller && teller.phone === credentials.phone) {

          // user phone number & device id match
          // so skip confirmation code and generate token
          teller.createAccessToken(86400, {tokenParams: { phone: credentials.phone, cc: credentials.cc }}, function(err, token) {
            if (err) return fn(err);
            token.__data.user = { 
              phone: credentials.phone, 
              cc: credentials.cc, 
              // uuid: credentials.uuid,
              balance: teller.balance
            };
            fn(null, token);
          });

        }
        else {
          var secret = APP_SECRET + credentials.phone;
          var code = speakeasy.totp({
            secret: secret
          });

          console.log('SMS code for ' + credentials.phone + ': ' + code);
          
          // [TODO] hook into your favorite SMS API and 
          //        send your user the code!
          var msg = "Your GeoPay confirmation code: " + code;

          try {
            click.sendmsg(msg,[credentials.phone],function(res){
              console.log(res); // ID: 4c640d23a882b331563a2a5dcab258a8 
              //return fn(null, now);
              return fn(null, { code: code, msgId: res, timestamp: now });
            });
          }
          catch(error) {
            return fn(error, { error: error, timestamp: now });
          }
        }
      });
  };
  
  Teller.remoteMethod(
      'requestCode',
      {
          description: 'Request a sms code for a user with phone, cc, and uuid',
          accepts: [
              {arg: 'credentials', type: 'object', required: true, http: {source: 'body'}}
          ],
          returns: {
              arg: 'resp',
              type: 'object',
              root: true,
              description: 'The response body contains properties of the AccessToken created on login.\n'
          },
          http: {verb: 'post'}
      }
  );

  /**
   * A method for logging in a user using a time-based (quickly expiring)
   * verification code obtained using the `requestCode()` method.
   * 
   * @param  {object}   credentials A JSON object containing "email" and "code" fields
   * @param  {Function} fn          The function to call in the Loopback for sending back data
   * @return {void}
   */
  Teller.loginWithCode = function(credentials, fn) {
      var self = this,
          defaultError = new Error('Login Failed');
      
      defaultError.statusCode = 401;
      defaultError.code = 'LOGIN_FAILED';

      console.log(JSON.stringify(credentials));
      
      if (!credentials.phone || !credentials.cc || !credentials.uuid || !credentials.code) {
          return fn(new Error('Login Failed: Missing Param'));
      }

      var secret = APP_SECRET + credentials.phone;
      var verified = speakeasy.totp.verify({
            secret: secret,
            token: credentials.code,
            window: 6
          });

      console.log("verified: " + verified);

      if (!verified) {
          return fn(new Error('Login Failed: Invalid Code'));
      }
      else {
        self.findOrCreate({where: { phone: credentials.phone }}, { phone: credentials.phone, cc: credentials.cc, uuid: credentials.uuid }, function(err, teller) {
          console.log("teller: " + JSON.stringify(teller));
          if (err) return fn(err);
          if (!teller) return fn(defaultError);

          teller.createAccessToken(86400, {tokenParams: { phone: credentials.phone, cc: credentials.cc }}, function(err, token) {
            if (err) return fn(err);
            token.__data.user = { 
              phone: credentials.phone, 
              cc: credentials.cc, 
              uuid: credentials.uuid,
              balance: teller.balance
            };
            fn(null, token);
          });
        });
      }
  };
  
  Teller.remoteMethod(
      'loginWithCode',
      {
          description: 'Login a user with phone, cc, uuid, and sms code',
          accepts: [
              {arg: 'credentials', type: 'object', required: true, http: {source: 'body'}}
          ],
          returns: {
              arg: 'resp',
              type: 'object',
              root: true,
              description: 'The response body contains properties of the AccessToken created on login.\n'
          },
          http: {verb: 'post'}
      }
  );

  // Teller.beforeRemote('deviceToken', function (context, unused, next) {
  //   var req = context.req;
  //   console.log("req.accessToken: " + req.accessToken);
  //   if(req.accessToken) {
  //     req.remotingContext.args.accessToken = req.accessToken;
  //     next();
  //   }
  //   else {
  //     var authError = new Error('Unauthorized');
  //     authError.statusCode = 401;
  //     authError.code = 'Unauthorized';
  //     next(authError);
  //   }
  // });

  // Teller.deviceToken = function(accessToken, args, cb) {
  //   console.log("accessToken: " + JSON.stringify(accessToken));
  //   var phone = accessToken.userId;
  //   var deviceToken = args.deviceToken;

  //   Teller.findOne({ where: { phone: phone }}, function(err, user) {
  //     console.log("user: " + JSON.stringify(user));
  //     if (err) cb(err);
  //     else {

  //       Teller.upsert({ phone: user.phone, cc: user.cc, deviceToken: deviceToken }, function(err, updatedUser) {
  //         console.log("updatedUser upsert: " + JSON.stringify(updatedUser));
  //         if (err) deferred.reject(err);
  //         else {
  //           pushNotifyRecipient(deviceToken);
  //           cb(null, updatedUser);
  //         }
  //       });
  //     }
  //   });
  // };

  // function pushNotifyRecipient(deviceToken) {
  //   var deferred = Q.defer();

  //   // Define relevant info
  //   var jwt = process.env['IONIC_PUSH_TOKEN'];
  //   var profile = process.env['IONIC_PUSH_PROFILE'];
  //   var tokens = [deviceToken];
  //   var message = "Test works";
  //   // Build the request object
  //   var req = {
  //     method: 'POST',
  //     url: 'https://api.ionic.io/push/notifications',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer ' + jwt
  //     },
  //     data: {
  //       "tokens": tokens,
  //       "profile": profile,
  //       "notification": {
  //         "message": message 
  //       }
  //     }
  //   };

  //   var authRequest = request.defaults({
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer ' + jwt
  //     }
  //   });

  //   var options = {
  //     uri: 'https://intense-reef-51024.herokuapp.com/remit',
  //     method: 'POST',
  //     json: {
  //       "tokens": tokens,
  //       "profile": profile,
  //       "notification": {
  //         "message": message 
  //       }
  //     }
  //   };

  //   authRequest(options, function (error, response, body) {
  //     console.log("error: " + JSON.stringify(error));
  //     console.log("response: " + JSON.stringify(response));
  //     console.log("body remit: " + JSON.stringify(body));
  //     if (!error && response.statusCode == 200) {
  //       var remit = body;
  //       if (remit.err) deferred.reject(new Error(remit.err));
  //       else deferred.resolve(remit.result); // resolve push notification
  //     }
  //     else {
  //       console.log("error: " + error);
  //       deferred.reject(error);
  //     }

  //   });

  //   return deferred.promise;
  // } // pushNotifyRecipient
  
  // Teller.remoteMethod(
  //     'deviceToken',
  //     {
  //         description: 'Save token for ionic push notifications',
  //         accepts: [
  //             {arg: 'accessToken', type: 'object'},
  //             {arg: 'deviceToken', type: 'string', required: true, http: {source: 'body'}}
  //         ],
  //         returns: {
  //             arg: 'resp',
  //             type: 'object',
  //             root: true,
  //             description: 'updated user with device token.\n'
  //         },
  //         http: {
  //           path: '/deviceToken',
  //           verb: 'post'
  //         }
  //     }
  // );

};
