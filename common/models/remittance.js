var app = require('../../server/server');
var click = require('../utils/clickatell');
var request = require('request');
var Q = require('q');
var accounting = require('accounting');
var moneyUtil = require('../utils/moneyUtil');
var Transaction = require('loopback-datasource-juggler').Transaction;

module.exports = function(Remittance) {

  Remittance.definition.rawProperties.created.default =
  Remittance.definition.properties.created.default = function() {
    return new Date();
  };


  Remittance.beforeRemote('recipient', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Remittance.recipient = function(accessToken, cb) {

      console.log("userId: " + accessToken.userId);

      Remittance.find({where: {recipientId: accessToken.userId} }, function(error, remittances, body) {
        console.log("remittances: " + JSON.stringify(remittances));
        if (error) { return cb(error); }
        return cb(null, remittances);
      });

    };

    // expose the above method through the REST
    Remittance.remoteMethod('recipient', {
        description: 'Get remittances received',
        accepts: {arg: 'accessToken', type: 'object'},
        returns: {
            arg: 'remittances',
            type: 'array'
        },
        http: {
            path: '/recipient',
            verb: 'get'
        }
    });

   Remittance.beforeRemote('sentAndReceived', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Remittance.sentAndReceived = function(accessToken, cb) {

    console.log("Remittance.find");
      console.log("userId: " + accessToken.userId);

      // TODO: 'OR' filter not working
      this.find({where: {or: [
        {senderId: accessToken.userId},
        {recipientId: accessToken.userId}
      ]}, order: 'lastUpdated DESC'}, function(error, remittances, body) {
        console.log("remittances: " + JSON.stringify(remittances));
        if (error) { return cb(error); }
        return cb(null, remittances);
      });

      // Remittance.find({where: 
      //   {senderId: accessToken.userId}
      // }, function(error, remittances, body) {
      //   if (error) { return cb(error); }
        
      //   Remittance.find({where: {recipientId: accessToken.userId}
      //   }, function(error, recRemittances, body) {
      //     console.log("remittances: " + JSON.stringify(remittances));
      //     if (error) { return cb(error); }
      //     var r = remittances.concat(recRemittances);
      //     return cb(null, r);
      //   });

      // });


    };

    // expose the above method through the REST
    Remittance.remoteMethod('sentAndReceived', {
        description: 'Get remittances received and sent',
        accepts: {arg: 'accessToken', type: 'object'},
        returns: {
            arg: 'remittances',
            type: 'array'
        },
        http: {
            path: '/teller',
            verb: 'get'
        }
    });


    Remittance.beforeRemote('new', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });

    /**
   * Request a two-factor authentication code for use during the login process
   * 
   * @param  {object}   credentials A JSON object containing "phone" and "cc" fields
   * @param  {Function} fn          The function to call in the Loopback for sending back data
   * @return {void}
   */
  Remittance.new = function(accessToken, remittance, cb) {
    console.log("accessToken: " + JSON.stringify(accessToken));

    console.log("senderId: " + accessToken.userId);
    console.log("remittance: " + JSON.stringify(remittance));

    if (moneyUtil.validate(remittance.senderAmount/100) === null) return cb(new Error("Invalid amount"));

    var phone = accessToken.userId;
    remittance.senderId = phone; // enforce logged in user is sending remittance
    var sender;

    // transaction
    var timeout = 9999999; // 30000ms = 30s
    var txRemittance;
    Remittance.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: timeout
    }, function(err, tx) {
      if (err) return cb(new Error(err));
      txRemittance = tx;
      // console.log("txRemittance: " + JSON.stringify(txRemittance));
      // tx.observe('timeout', function(context, next) {
      //   tx.rollback(function(err) {
      //     if (err) return cb(new Error(err));
      //     //return cb(new Error("Transaction timed out"));
      //   });
      //   //next();
      // });
    });

    // TODO: Add Transactions
    // https://docs.strongloop.com/display/public/LB/Using+database+transactions
    getSender(phone, txRemittance)
    .then(function(updatedSender) {
      sender = updatedSender;
      console.log("sender: " + JSON.stringify(sender));
      return confirmFunds(sender, remittance);
    })
    .then(function() {
      if (remittance.senderCc != remittance.recipientCc) {
        // international transfer via bitcoin
        return sendRemittance(sender, remittance, txRemittance).then(function(bitcoinRemittance) {
          return logRemittance(sender, bitcoinRemittance, remittance, txRemittance);
        });
      }
      else {
        // domestic transfer can skip the bitcoin step
        remittance.recipientAmount = remittance.senderAmount;
        return logTransfer(sender, remittance, txRemittance);
      }
    })
    .then(function(updatedRemittance) {
      remittance = updatedRemittance; // after recipient fields have been updated
      console.log("deductSenderBalance sender: " + JSON.stringify(sender));
      return deductSenderBalance(sender, remittance, txRemittance);
    })
    .then(function(updatedSender) {
      sender = updatedSender; // after balance has been updated
      return addRecipientBalance(remittance, txRemittance);
    })
    .then(function() {
      return smsRecipient(remittance);
      //return pushNotifyRecipient(remittance);
    })
    .then(function() {
      txRemittance.commit(function(err) {
        if (err) { cb(new Error("Transaction unsuccessful. Try again.")); }
        else {
          cb(null, {
            sender: sender,
            remittance: remittance
          });
        }
      });
    })
    .catch(function(error) {
      console.log("Error: " + error);
      txRemittance.rollback(function(err) {
        console.log("transaction err: " + err);
        cb(error);
      });
    });
    

  };

  function getSender(phone, tx) {
    var deferred = Q.defer();

    Remittance.app.models.Teller.findOne({where: { phone: phone }}, {transaction: tx}, function(err, sender) {
      if (err) deferred.reject(err);
      if (!sender) deferred.reject(new Error("Sender does not exist")); // not possible?
      else deferred.resolve(sender);
    });

    return deferred.promise;
  } // getSender

  function confirmFunds(sender, remittance) {
    var deferred = Q.defer();

    if (sender.balance >= remittance.senderAmount) {
      deferred.resolve(sender);
    }
    else {
      var error = new Error('Insufficient Funds');
      //error.statusCode = 401;
      error.code = 'INSUFFICIENT_FUNDS';
      deferred.reject(error);
    }

    return deferred.promise;
  } // checkBalance

  function sendRemittance(sender, remittance) {
    var deferred = Q.defer();

    var authRequest = request.defaults({
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6InN5c3RlbSIsImlhdCI6MTQwOTc1MDA5MH0.bvAspdemmh5e0NEBJTJ_Ya6K24Kb8L26h4p3KJ4PwW4'
      }
    });

    var options = {
      uri: 'https://intense-reef-51024.herokuapp.com/remit',
      method: 'POST',
      json: {
        "amount" : remittance.senderAmount,
        "from" : remittance.senderCc.toUpperCase(),
        "to" : remittance.recipientCc.toUpperCase()
      }
    };

    authRequest(options, function (error, response, body) {
      console.log("error: " + JSON.stringify(error));
      console.log("response: " + JSON.stringify(response));
      console.log("body remit: " + JSON.stringify(body));
      if (!error && response.statusCode == 200) {
        var remit = body;
        if (remit.err) deferred.reject(new Error(remit.err));
        else deferred.resolve(remit.result); // resolve bitcoinRemittance
      }
      else {
        console.log("error: " + error);
        deferred.reject(error);
      }

    });

    return deferred.promise;
  } // sendRemittance

  function deductSenderBalance(sender, remittance, tx) {
    var deferred = Q.defer();

    var balance = sender.balance - remittance.senderAmount;

    Remittance.app.models.Teller.upsert({ phone: sender.phone, cc: sender.cc, balance: balance }, {transaction: tx}, function(err, sender) {
      console.log("sender: " + JSON.stringify(sender));
      if (err) deferred.reject(err);
      else deferred.resolve(sender);
    });

    return deferred.promise;
  } // deductSenderBalance

  function addRecipientBalance(remittance, tx) {
    var deferred = Q.defer();

    var balance = 0.00;

    Remittance.app.models.Teller.findOne({ where: { phone: remittance.recipientId }}, {transaction: tx}, function(err, recipient) {
      console.log("recipient: " + JSON.stringify(recipient));
      if (err) deferred.reject(err);
      else {
        if(recipient) balance = recipient.balance;

        balance = balance + remittance.recipientAmount;
        //balance = ""+balance;

        Remittance.app.models.Teller.upsert({ phone: remittance.recipientId, cc: remittance.recipientCc, balance: balance }, {transaction: tx}, function(err, recipient) {
          console.log("recipient upsert: " + JSON.stringify(recipient));
          if (err) deferred.reject(err);
          else deferred.resolve(recipient);
        });
      }
    });

    return deferred.promise;
  } // addRecipientBalance

  function smsRecipient(remittance) {
    var deferred = Q.defer();

    var url = "http://bit.ly/1R4K24X";
    var msg = "You have received money from " + remittance.senderId + "! Download GeoPay to collect " + url;
    click.sendmsg(msg,[remittance.recipientId],function(res){
      console.log(res); // ID: 4c640d23a882b331563a2a5dcab258a8 
      deferred.resolve(res)
    });

    return deferred.promise;
  } // smsRecipient

  function pushNotifyRecipient(remittance) {
    var deferred = Q.defer();

    // Define relevant info
    var jwt = process.env['IONIC_PUSH_TOKEN'];
    var profile = process.env['IONIC_PUSH_PROFILE'];
    var tokens = [remittance.pushDeviceToken];
    var message = "You received " + remittance.recipientAmount;
    // Build the request object
    var req = {
      method: 'POST',
      url: 'https://api.ionic.io/push/notifications',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      data: {
        "tokens": tokens,
        "profile": profile,
        "notification": {
          "message": message 
        }
      }
    };

    var authRequest = request.defaults({
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      }
    });

    var options = {
      uri: 'https://intense-reef-51024.herokuapp.com/remit',
      method: 'POST',
      json: {
        "tokens": tokens,
        "profile": profile,
        "notification": {
          "message": message 
        }
      }
    };

    authRequest(options, function (error, response, body) {
      console.log("error: " + JSON.stringify(error));
      console.log("response: " + JSON.stringify(response));
      console.log("body remit: " + JSON.stringify(body));
      if (!error && response.statusCode == 200) {
        var remit = body;
        if (remit.err) deferred.reject(new Error(remit.err));
        else deferred.resolve(remit.result); // resolve push notification
      }
      else {
        console.log("error: " + error);
        deferred.reject(error);
      }

    });

    return deferred.promise;
  } // pushNotifyRecipient

  function logRemittance(sender, bitcoinRemittance, remittance, tx) {
    var deferred = Q.defer();

    var recipientAmount = Math.floor(100 * (bitcoinRemittance.sell_transaction.counter - bitcoinRemittance.sell_transaction.fee_counter));
    var recipientCurrency = bitcoinRemittance.sell_transaction.pair.slice(3);

    var newRemittance = {
      senderId: remittance.senderId,
      senderAmount: remittance.senderAmount,
      senderCc: remittance.senderCc,
      senderCurrency: remittance.senderCurrency,
      recipientId: remittance.recipientId,
      recipientAmount: recipientAmount,
      recipientCc: bitcoinRemittance.country_to.toLowerCase(),
      recipientCurrency: recipientCurrency 
    };

    Remittance.create(newRemittance, {transaction: tx}, function(error, newRemittance) {
      if (error) deferred.reject(error);
      else deferred.resolve(newRemittance);
    });

    return deferred.promise;
  } // logRemittance

  function logTransfer(sender, remittance, tx) {
    var deferred = Q.defer();

    console.log("remittance.senderId: " + remittance.senderId);
    var newRemittance = {
      senderId: remittance.senderId,
      senderAmount: remittance.senderAmount,
      senderCc: remittance.senderCc,
      senderCurrency: remittance.senderCurrency,
      recipientId: remittance.recipientId,
      recipientAmount: remittance.senderAmount,
      recipientCc: remittance.senderCc,
      recipientCurrency: remittance.senderCurrency 
    };

    Remittance.create(newRemittance, {transaction: tx}, function(error, newRemittance) {
      if (error) deferred.reject(error);
      else deferred.resolve(newRemittance);
    });

    return deferred.promise;
  } // logTransfer

  Remittance.remoteMethod(
      'new',
      {
          description: 'Send a remittance',
          accepts: [
              {arg: 'accessToken', type: 'object'},
              {arg: 'remittance', type: 'object', required: true, http: {source: 'body'}}
          ],
          returns: {
              arg: 'resp',
              type: 'object',
              root: true,
              description: 'The response body contains the remittance.\n'
          },
          http: {
            path: '/new',
            verb: 'post'
        }
      }
  );
};
