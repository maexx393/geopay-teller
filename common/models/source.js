var Q = require('q');
var plaid = require('plaid');

var STRIPE_API_SECRET_KEY = process.env.STRIPE_API_SECRET_KEY;
var stripe = require("stripe")(STRIPE_API_SECRET_KEY);

var FUNDING_TYPE = {
  CARD: 1,
  BANK: 2
};

module.exports = function(Source) {

  Source.beforeRemote('addPlaidBank', function (context, unused, next) {
    var req = context.req;
    if(req.accessToken) {
      req.remotingContext.args.accessToken = req.accessToken;
      next();
    }
    else {
      var authError = new Error('Unauthorized');
      authError.statusCode = 401;
      authError.code = 'Unauthorized';
      next(authError);
    }
  });


  Source.addPlaidBank = function addPlaidBank(accessToken, data, cb) {

    console.log("phone: " + accessToken.userId);
    console.log("data: " + JSON.stringify(data));

    var teller = {
      phone: accessToken.userId
    };

    var bank = {};

    getStripeCustId(teller).then(function(profile) {
      if (profile && profile.stripeCustomerId) {
        // use existing stripe customer account

        teller.stripeCustomerId = profile.stripeCustomerId;

        return getStripeBankToken(data.token, data.accountId).then(function(tokenObj) {
          console.log("Existing stripe customer: " + JSON.stringify(tokenObj));

          return createStripeBankAccount(teller.stripeCustomerId, tokenObj.stripeBankToken);
        });
      }
      else {
        // otherwise create stripe account and save it first
        return getStripeBankToken(data.token, data.accountId).then(function(tokenObj) {
          var stripeToken = tokenObj.stripeBankToken;
          
          return stripeCreateCustomer(teller, stripeToken).then(function(customer) {

            teller.stripeCustomerId = customer.id;
            bank.id = customer.sources.data[0].id; // new customer only has new (bank) source
            
            return saveStripeCustId(teller).then(function() {
              
              return getBankDetails(teller.stripeCustomerId, bank.id);
            })
          })
        })
      }
    })
    .then(function(stripeBankDetails) {              
      return saveBankAccount(teller, stripeBankDetails);
    })
    .then(function(savedBank) {
      console.log("savedBank: " + savedBank);
      bank = savedBank;
    })
    .catch(function(error) {
        console.log("Error: " + error);
        cb(error);
      })
      .done(function() {
        cb(null, {
          teller: teller,
          bank: bank
        });
      });
  };

  function createStripeBankAccount(stripeCustId, stripeBankToken) {
    var deferred = Q.defer();

    stripe.customers.createSource(
      stripeCustId,
      {source: stripeBankToken},
      function(err, bank) {
        if (err) {
          console.log("createStripeBankAccount err: " + JSON.stringify(err));
          deferred.reject(new Error(err.message));
        } 
        else {
          console.log("createStripeBankAccount: " + JSON.stringify(bank));
          deferred.resolve(bank);
        } 
      }
    )

    return deferred.promise;
  }

  function getFundingSource(teller, stripeId) {
    var deferred = Q.defer();

    Source.findOne({
      where: {
        phone: teller.phone,
        stripeId: stripeId
      }
    },
    function(err, source) {
      if(err) {
        console.log("getFundingSource error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("getFundingSource:", source);
        deferred.resolve(source);
      }
    });

    return deferred.promise;
  }

  function getBankDetails(stripeCustId, stripeBankId) {
    var deferred = Q.defer();

    stripe.customers.retrieveSource(stripeCustId, stripeBankId,
      function(err, bank) {
        if (err) {
          console.log("getBankDetails err: " + JSON.stringify(err));
          deferred.reject(new Error("Error Retrieving Bank Details"));
        } 
        else {
          console.log("getBankDetails: " + JSON.stringify(bank));
          deferred.resolve(bank);
        } 
      }
    )

    return deferred.promise;
  }

  function getStripeBankToken(plaidToken, plaidAccountId) {
    var deferred = Q.defer();

    var plaidEnv = (process.env['PLAID_ENV'] === "tartan") ? plaid.environments.tartan : plaid.environments.production;
    var plaidClient = new plaid.Client(process.env['PLAID_CLIENT_ID'],
                                   process.env['PLAID_SECRET_KEY'],
                                   plaidEnv);

    plaidClient.exchangeToken(plaidToken, plaidAccountId, function(err, res) {
      if (err) {
        console.log("err: " + JSON.stringify(err));
        deferred.reject(new Error("Unable to Link Bank"));
      }
      else {
        console.log("res: " + JSON.stringify(res));

        // Use the access_token to make make Plaid API requests.
        // Use the stripe_bank_account_token to make Stripe ACH API requests.
        deferred.resolve({
          plaidToken: res.access_token,
          stripeBankToken: res.stripe_bank_account_token
        });
      }
    });

    return deferred.promise;
  }

  function saveBankAccount(teller, bank) {
    console.log("start saveBankAccount");
    var deferred = Q.defer();

    Source.findOrCreate({
      where: {
        phone: teller.phone,
        stripeId: bank.id
      }
    },
    {
      phone: teller.phone,
      type: FUNDING_TYPE.BANK,
      stripeId: bank.id,
      status: bank.status,
      bankName: bank.bank_name,
      last4: bank.last4 }, 
      function(err, source) {
      if(err) {
        console.log("saveBankAccount error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("saveBankAccount:", source);
        deferred.resolve(source);
      }
    });

    return deferred.promise;
  }

  // expose the above method through the REST
  Source.remoteMethod('addPlaidBank', {
    description: 'Link plaid to stripe customer',
    accepts: [
          {arg: 'accessToken', type: 'object'},
          {arg: 'amount', type: 'object', required: true, http: {source: 'body'}}
      ],
    returns: {
        arg: 'stripe',
        type: 'object',
        root: true
    },
    http: {
        path: '/add-plaid-bank',
        verb: 'post'
    }
  });

  Source.beforeRemote('storeStripeBank', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Source.storeStripeBank = function storeStripeBank(accessToken, data, cb) {

    console.log("phone: " + accessToken.userId);
    console.log("token: " + data.token);
    
    var teller =  {
      phone: accessToken.userId
    };
    var token = data.token;
    var card;
    var source;

    getStripeCustId(teller)
    .then(function(profile) {
      if (profile && profile.stripeCustomerId) {

        teller.stripeCustomerId = profile.stripeCustomerId;

        return addStripeSource(teller, token).then(function(card) {

          return saveStripeBank(teller, card);
        })

      }
      else {

        return stripeCreateCustomer(teller, token).then(function(customer) {
          teller.stripeCustomerId = customer.id;
          card = customer.sources.data[0]; // new customer only has new (card) source

          return saveStripeCustId(teller).then(function(profile) {
            teller.stripeCustomerId = profile.stripeCustomerId;

            return saveStripeBank(teller, card);

          })
        })
      }
    })  
    .then(function(fundingSource) {
      console.log("source: " + JSON.stringify(fundingSource));
      source = fundingSource; 
    })
    .catch(function(error) {
      console.log("Error: " + error);
      cb(error);
    })
    .done(function() {
      cb(null, {
        teller: teller,
        source: source
      });
    });
  };

  // expose the above method through the REST
  Source.remoteMethod('storeStripeBank', {
    description: 'Link bank to stripe customer',
    accepts: [
          {arg: 'accessToken', type: 'object'},
          {arg: 'data', type: 'object', required: true, http: {source: 'body'}}
      ],
    returns: {
        arg: 'stripe',
        type: 'object',
        root: true
    },
    http: {
        path: '/add-stripe-bank',
        verb: 'post'
    }
  });

  Source.beforeRemote('verifyStripeBank', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Source.verifyStripeBank = function verifyStripeBank(accessToken, data, cb) {

    console.log("phone: " + accessToken.userId);
    console.log("data: " + JSON.stringify(data));
    
    var teller =  {
      phone: accessToken.userId
    };
    var source;

    getStripeCustId(teller)
    .then(function(profile) {
      teller.stripeCustomerId = profile.stripeCustomerId;
      return verifySource(teller.stripeCustomerId, data.bankId, data.verify);
    })  
    // TODO, use customer id, bank id, and verify amounts to verify account, then update source status
    .then(function(bank) {
      console.log("bank: " + JSON.stringify(bank));
      source = bank;
      return updateSourceStatus(bank);
    })
    .catch(function(error) {
      console.log("Error: " + error);
      cb(error);
    })
    .done(function() {
      cb(null, {
        teller: teller,
        bank: source
      });
    });
  };

  // expose the above method through the REST
  Source.remoteMethod('verifyStripeBank', {
    description: 'Verify bank to stripe customer',
    accepts: [
          {arg: 'accessToken', type: 'object'},
          {arg: 'data', type: 'object', required: true, http: {source: 'body'}}
      ],
    returns: {
        arg: 'stripe',
        type: 'object',
        root: true
    },
    http: {
        path: '/verify-stripe-bank',
        verb: 'post'
    }
  });

  Source.beforeRemote('storeStripeCard', function (context, unused, next) {
      var req = context.req;
      if(req.accessToken) {
        req.remotingContext.args.accessToken = req.accessToken;
        next();
      }
      else {
        var authError = new Error('Unauthorized');
        authError.statusCode = 401;
        authError.code = 'Unauthorized';
        next(authError);
      }
  });


  Source.storeStripeCard = function storeStripeCard(accessToken, data, cb) {

    console.log("phone: " + accessToken.userId);
    console.log("token: " + data.token);
    
    var teller =  {
      phone: accessToken.userId
    };
    var token = data.token;
    var card;
    var source;

    getStripeCustId(teller)
    .then(function(profile) {
      if (profile && profile.stripeCustomerId) {

        teller.stripeCustomerId = profile.stripeCustomerId;

        return addStripeSource(teller, token).then(function(card) {

          return saveStripeCard(teller, card);
        })

      }
      else {

        return stripeCreateCustomer(teller, token).then(function(customer) {
          teller.stripeCustomerId = customer.id;
          card = customer.sources.data[0]; // new customer only has new (card) source

          return saveStripeCustId(teller).then(function(profile) {
            teller.stripeCustomerId = profile.stripeCustomerId;

            return saveStripeCard(teller, card);

          })
        })
      }
    })  
    .then(function(fundingSource) {
      console.log("source: " + JSON.stringify(fundingSource));
      source = fundingSource; 
    })
    .catch(function(error) {
      console.log("Error: " + error);
      cb(error);
    })
    .done(function() {
      cb(null, {
        teller: teller,
        source: source
      });
    });
  };

  // expose the above method through the REST
  Source.remoteMethod('storeStripeCard', {
    description: 'Link plaid to stripe customer',
    accepts: [
          {arg: 'accessToken', type: 'object'},
          {arg: 'data', type: 'object', required: true, http: {source: 'body'}}
      ],
    returns: {
        arg: 'stripe',
        type: 'object',
        root: true
    },
    http: {
        path: '/add-stripe-card',
        verb: 'post'
    }
  });

  function updateSourceStatus(bank) {
    console.log("updateSourceStatus: ", bank.status)

    var deferred = Q.defer();

    Source.updateAll({
      stripeId: bank.id
    },
    {
      status: bank.status }, 
      function(err, updateCount) {
      if(err) {
        console.log("updateSourceStatus: error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("updateSourceStatus:", updateCount);
        deferred.resolve(updateCount);
      }
    });

    return deferred.promise;
  }

  function verifySource(stripeCustId, bankId, verify) {
    var deferred = Q.defer();

    stripe.customers.verifySource(
      stripeCustId,
      bankId,
      {
        amounts: verify
      },
      function(err, bankAccount) {
        if (err) {
          console.log("verifySource err: " + JSON.stringify(err));
          deferred.reject(new Error(err));
        }
        else {
          console.log("verifySource: " + JSON.stringify(bankAccount));
          deferred.resolve(bankAccount);
        }
    });

    return deferred.promise;
  }

  function getStripeCustId(teller) {
    var deferred = Q.defer();

    Source.app.models.Profile.findOne({phone: teller.phone} , function(err, profile) {
      if(err) {
        console.log("getStripeCustId: error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("getStripeCustId:", profile);
        deferred.resolve(profile);
      }
    });

    return deferred.promise;
  }

  function saveStripeCustId(teller) {
    var deferred = Q.defer();

    Source.app.models.Profile.upsert({phone: teller.phone, stripeCustomerId: teller.stripeCustomerId}, function(err, profile) {
      if(err) {
        console.log("save profile callback: error: " +  JSON.stringify(err));
        deferred.reject(new Error("Unable to Save Customer"));
      } else {
        console.log("save profile callback: " + JSON.stringify(profile));
        deferred.resolve(profile);
      }
    });

    return deferred.promise;
  }

  function stripeCreateCustomer(teller, stripeToken) {
    var deferred = Q.defer();

    stripe.customers.create({
      source:       stripeToken,
      description:  teller.phone
    }).then(function(customer) {
      console.log("stripeCreateCustomer: " + JSON.stringify(customer));
      if (customer) deferred.resolve(customer);
      else deferred.reject(new Error("No Customer Found"));
    });

    return deferred.promise;
  }

  function addStripeSource(teller, token) {
    var deferred = Q.defer();

    stripe.customers.createSource(teller.stripeCustomerId, {
      source: token
    }, function(err, card) {
      if (err) {
        console.log("addStripeSource err: " + JSON.stringify(err));
        deferred.reject(new Error("Unable to Add Source"));
      }
      else deferred.resolve(card); 
    });

    return deferred.promise;
  }

  function saveStripeBank(teller, bank) {
    var deferred = Q.defer();

    Source.findOrCreate({
      where: {
        phone: teller.phone,
        stripeId: bank.id
      }
    },
    {
      phone: teller.phone,
      type: FUNDING_TYPE.BANK,
      bankName: bank.bank_name,
      status: bank.status,
      stripeId: bank.id,
      last4: bank.last4 }, 
      function(err, source) {
      if(err) {
        console.log("saveStripeBank: error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("saveStripeBank:", source);
        deferred.resolve(source);
      }
    });

    return deferred.promise;
  }

  function saveStripeCard(teller, card) {
    var deferred = Q.defer();

    Source.findOrCreate({
      where: {
        phone: teller.phone,
        stripeId: card.id
      }
    },
    {
      phone: teller.phone,
      type: FUNDING_TYPE.CARD,
      stripeId: card.id,
      funding: card.funding,
      last4: card.last4,
      brand: card.brand }, 
      function(err, source) {
      if(err) {
        console.log("saveStripeCard: error:", err)
        deferred.reject(new Error(err));
      } else {
        console.log("saveStripeCard:", source);
        deferred.resolve(source);
      }
    });

    return deferred.promise;
  }

  Source.getPlaidInstitutions = function addPlaidBank(cb) {
    var plaidEnv = (process.env['PLAID_ENV'] === "tartan") ? plaid.environments.tartan : plaid.environments.production;

    plaid.getInstitutions(plaidEnv, function(err, response) {
      if (err) {
        console.log("getPlaidInstitutions err: " + JSON.stringify(err));
        cb(new Error("Unable to Get Institutions"));
      }
      else {
        //console.log("getPlaidInstitutions: " + JSON.stringify(response));
        cb(null, response);
      }
    });

  };

// expose the above method through the REST
  Source.remoteMethod('getPlaidInstitutions', {
    description: 'Get banks',
    accepts: [],
    returns: {
        arg: 'stripe',
        type: 'array',
        root: true
    },
    http: {
        path: '/plaid-institutions',
        verb: 'get'
    }
  });

};
