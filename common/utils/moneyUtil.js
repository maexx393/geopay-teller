module.exports = {
  validate: validate
};

function validate(value) {
  var value= ""+value;
  var regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
  if (regex.test(value)) {

    //Input is valid, check the number of decimal places
    var twoDecimalPlaces = /\.\d{2}$/g;
    var oneDecimalPlace = /\.\d{1}$/g;
    var noDecimalPlacesWithDecimal = /\.\d{0}$/g;
    
    if(value.match(twoDecimalPlaces ))
    {
        //all good, return as is
        return value;
    }
    if(value.match(noDecimalPlacesWithDecimal))
    {
        //add two decimal places
        return value+'00';
    }
    if(value.match(oneDecimalPlace ))
    {
        //ad one decimal place
        return value+'0';
    }
    //else there is no decimal places and no decimal
    return value+".00";
  }
  return null;
};

