// https://github.com/strongloop/loopback-example-database/tree/postgresql
var server = require('./server');
var ds = server.dataSources['GeoPay'];
var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];

// ds.autoupdate(lbTables, function(er) {
//   if (er) throw er;
//   console.log('Loopback table [' + lbTables + '] created in ', ds.adapter.name);
//   ds.disconnect();
// });


// hack due to limited db connections (e.g. free plan)
// do 1 at a time
update(lbTables[0])

function update(table, cb) {
  // autoupdate = refresh
  // automigrate = remove and add
  ds.autoupdate(table, function(er) {
    if (er) throw er;
    console.log('Loopback table [' + table + '] created in ', ds.adapter.name);
    ds.disconnect();
  });
}